package at.ac.tuwien.stp.spanningtree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public abstract class InstanceFileReader {
	public static Object[] readInstanceFile(String instanceFilename) {
		File instanceFile = new File(instanceFilename);
		return readInstanceFile(instanceFile);
	}

	private static Object[] readInstanceFile(File file) {
		Scanner scanner = null;

		try {
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int[][] adjacencyMatrix = null;
		Set<Integer> terminals = null;
		
		try {
			readHeader(scanner);
			while (scanner.hasNext("(?i)section")) {
				scanner.next("(?i)section");
				String sectionName = scanner.next("\\w+");
				if (sectionName.matches("(?i)graph")) {
					adjacencyMatrix = processGraphSection(scanner);
				} else if (sectionName.matches("(?i)terminals")) {
					terminals = processTerminalsSection(scanner);
				} else if (sectionName.matches("(?i)comment")) {
					while(!scanner.hasNext("(?i)end")) {
						scanner.nextLine();
					}
					scanner.next("(?i)end"); // skip coordinates section
				} else if (sectionName.matches("(?i)coordinates")) {
					while(!scanner.hasNext("(?i)end")) {
						scanner.nextLine();
					}
					scanner.next("(?i)end"); // skip comment section
				} else {
					throw new IOException("Uknown section encountered!");
				}
			}
			scanner.next("(?i)eof");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		scanner.close();
		
		return new Object[] {adjacencyMatrix, terminals};
	}

	private static void readHeader(Scanner scanner) throws IOException {
		scanner.nextLine();
	}

	private static int[][] processGraphSection(Scanner scanner) throws IOException {
		scanner.next("(?i)nodes");
		int numNodes = scanner.nextInt();
		
		int[][] adjacencyMatrix = new int[numNodes][numNodes];
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			for (int j = 0; j < adjacencyMatrix[i].length; j++) {
				adjacencyMatrix[i][j] = -1;
			}
		}
		
		scanner.next("(?i)edges");
		int numEdges = scanner.nextInt();
		
		for(int i = 0; i < numEdges; i++) {
			scanner.next("(?i)e");
			int v = scanner.nextInt();
			if(v < 1 || v > numNodes) {
				throw new IOException();
			}
			int w = scanner.nextInt();
			if(w < 1 || w > numNodes) {
				throw new IOException();
			}
			int weight = scanner.nextInt();
			if(weight < 1) {
				throw new IOException();
			}
			adjacencyMatrix[v - 1][w - 1] = weight;
			adjacencyMatrix[w - 1][v - 1] = weight;
		}
		scanner.next("(?i)end");
		
		return adjacencyMatrix;
	}

	private static Set<Integer> processTerminalsSection(Scanner scanner) throws IOException {
		scanner.next("(?i)terminals");
		int numTerminals = scanner.nextInt();
		Set<Integer> terminals = new HashSet<Integer>();
		for(int i = 0; i < numTerminals; i++) {
			scanner.next("(?i)t");
			int terminal = scanner.nextInt();
			if(terminal < 1) {
				throw new IOException();
			}
			terminals.add(terminal);
		}
		scanner.next("(?i)end");
		
		return terminals;
	}
}
