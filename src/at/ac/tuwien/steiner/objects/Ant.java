package at.ac.tuwien.steiner.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Ant {

	private int id;
	private boolean stopped;
	private List<Integer> visitedNodes;
	private int distanceTraveled;
	private Set<Edge> edgesCrossed;
	private Set<Edge> edgesFromMergedAnts;

	private int currentNode;

	public Ant(int startingNode) {
		this.id = startingNode;
		currentNode = startingNode;
		stopped = false;
		visitedNodes = new ArrayList<Integer>();
		visitedNodes.add(startingNode);
		edgesCrossed = new HashSet<Edge>();
		edgesFromMergedAnts = new HashSet<Edge>();
		distanceTraveled = 0;
	}

	public int getId() {
		return id;
	}

	public void moveFromTo(int i, int j, int distance) {
		edgesCrossed.add(new Edge(i, j));
		visitedNodes.add(j);
		currentNode = j;
		distanceTraveled += distance;
	}

	public void stop() {
		stopped = true;
	}

	public boolean isStopped() {
		return stopped;
	}

	public int getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(int n) {
		this.currentNode = n;
	}

	public int getDistanceTraveled() {
		return distanceTraveled;
	}

	public Set<Edge> getEdgesCrossed() {
		return edgesCrossed;
	}

	public List<Integer> getVisitedNodes() {
		return visitedNodes;
	}

	public void mergePathWith(Ant a) {
		edgesFromMergedAnts.addAll(a.getEdgesCrossed());
		edgesFromMergedAnts.addAll(a.getEdgesFromMergedAnts());
	}

	public Set<Edge> getEdgesFromMergedAnts() {
		return edgesFromMergedAnts;
	}

}
