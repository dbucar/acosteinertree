package at.ac.tuwien.steiner.objects;

import java.util.Set;

public class Solution {

	private Set<Edge> edges;
	private int fitness;

	public Solution(Set<Edge> edges, int fitness) {
		this.edges = edges;
		this.fitness = fitness;
	}

	public int getFitness() {
		return fitness;
	}

	public Set<Edge> getEdges() {
		return edges;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;

	}

	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}
}
