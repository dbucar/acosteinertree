package at.ac.tuwien.steiner.objects;

public class Edge {

	private int i;
	private int j;

	public Edge(int i, int j) {
		this.i = i;
		this.j = j;
	}

	public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Edge))
			return false;
		Edge e = (Edge) o;
		return ((e.getI() == this.getI() && e.getJ() == this.getJ()) || (e.getI() == this.getJ() && e.getJ() == this
				.getI()));
	}

	@Override
	public int hashCode() {
		final int prime1 = 31, prime2 = 57;
		if (this.getI() < this.getJ()) {
			return prime1 * this.getI() + prime2 * this.getJ();
		} else {
			return prime2 * this.getI() + prime1 * this.getJ();
		}
	}

}
