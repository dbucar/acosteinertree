package at.ac.tuwien.steiner;

import java.util.Set;

import org.apache.log4j.Logger;

import at.ac.tuwien.steiner.InstanceFileReader;
import at.ac.tuwien.steiner.objects.Edge;
import at.ac.tuwien.steiner.objects.Solution;
import at.ac.tuwien.steiner.solver.ants.ACOSolver;

public class Main2 {

	private static Logger log = Logger.getLogger("at.ac.tuwien.steiner.Main2");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Object[] data = InstanceFileReader.readInstanceFile(args[0]);
		int[][] m = (int[][]) data[0];
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				System.out.print(m[i][j] == -1 ? "X " : m[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println((Set<Integer>) data[1]);

		log.info("==========================================================");
		log.info(args[0]);
		ACOSolver solver = new ACOSolver((int[][]) data[0], (Set<Integer>) data[1]);
		Solution s = solver.solveIt();

		String string = "";
		for (Edge e : s.getEdges()) {
			string += "[" + e.getI() + "," + e.getJ() + "] ";
		}
		log.info(string);
		log.info("is connected: " + solver.checkConnectivity(s));
		log.info("==========================================================");
		log.info("");
	}
}
