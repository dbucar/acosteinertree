package at.ac.tuwien.steiner.solver;

import at.ac.tuwien.steiner.objects.Solution;

public interface Solver {
	
	public Solution solveIt();

}
