package at.ac.tuwien.steiner.solver.ants;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import at.ac.tuwien.steiner.objects.Edge;
import at.ac.tuwien.steiner.objects.Solution;
import at.ac.tuwien.steiner.solver.Solver;

public class ACOSolver implements Solver {

	private static Logger log = Logger.getLogger("at.ac.tuwien.steiner.ACOSolver");

	private final int MAXITER = 100000;
	private final int alpha = 1;
	private final int beta = 2;

	private AntSolutionGenerator asg;
	private PheromoneModel pm;
	private int[][] adjacencyMatrix;
	private Set<Integer> terminals;

	public ACOSolver(int[][] adjacencyMatrix, Set<Integer> terminals) {
		this.adjacencyMatrix = adjacencyMatrix;
		this.terminals = terminals;
		this.asg = new AntSolutionGenerator(adjacencyMatrix, terminals, alpha, beta);
		this.pm = new PheromoneModel(adjacencyMatrix);
	}

	public Solution solveIt() {

		double[][] pheromones = pm.initPheromones();

		Solution solution = null, bestSolution = null;
		int bestFitness = Integer.MAX_VALUE;

		int iteration = 1;
		do {
			// refresh pheromones & add a bit for bestSolution
			if (iteration % 100 == 0) {
				pheromones = pm.initPheromones();
				pheromones = pm.updatePheromones(pheromones, bestSolution);
			}
			
			if (iteration % 1000 == 0) {
				log.info("iter " + iteration + " | current best: " + bestFitness);
			}

			solution = asg.generateSolution(pheromones);
			solution.setFitness(solution.getFitness() + connectedPenalty(solution));

			if (solution.getFitness() < bestFitness) {
				bestFitness = solution.getFitness();
				bestSolution = solution;
				log.info("iter " + iteration + " | new best: " + bestFitness);
				String string = "";
				for (Edge e : bestSolution.getEdges()) {
					string += "[" + e.getI() + "," + e.getJ() + "] ";
				}
				log.info(string);
			}

			pheromones = pm.evaporatePheromones(pheromones);
			pheromones = pm.updatePheromones(pheromones, solution);

			iteration++;
		} while (iteration < MAXITER);

		return bestSolution;

	}

	private int connectedPenalty(Solution solution) {

		if (checkConnectivity(solution))
			return 0;

		return 200;
	}

	public boolean checkConnectivity(Solution solution) {

		int[][] m = new int[adjacencyMatrix.length][adjacencyMatrix.length];

		for (Edge e : solution.getEdges()) {
			m[e.getI()][e.getJ()] = 1;
			m[e.getJ()][e.getI()] = 1;
		}

		Set<Integer> visited = new HashSet<Integer>();
		Set<Integer> reachable = new HashSet<Integer>();

		reachable.add(terminals.iterator().next());
		boolean changed;
		do {
			changed = false;
			Set<Integer> temp = new HashSet<Integer>();
			for (Integer vertex : reachable) {
				if (!visited.contains(vertex)) {
					for (int i = 0; i < m.length; i++) {
						if (m[vertex][i] != 0) {
							temp.add(i);
						}
					}
					visited.add(vertex);
					changed = true;
				}
			}
			reachable.addAll(temp);
		} while (changed);

		return reachable.containsAll(terminals);
	}

	private Set<Edge> removeNonTerminalLeaves(Set<Edge> edges) {

		int[][] m = new int[adjacencyMatrix.length][adjacencyMatrix.length];
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {
				m[i][j] = 0;
			}
		}

		for (Edge e : edges) {
			m[e.getI()][e.getJ()] = 1;
			m[e.getJ()][e.getI()] = 1;
		}

		boolean changed = true;

		while (changed) {
			changed = false;
			for (int i = 0; i < m.length; i++) {
				if (!terminals.contains(i)) {
					int adjVertices = 0;
					int neighbor = -1;
					for (int j = 0; j < m.length; j++) {
						if (m[i][j] != 0) {
							neighbor = j;
							adjVertices++;
						}
					}
					if (adjVertices == 1) {
						m[i][neighbor] = 0;
						m[neighbor][i] = 0;
						changed = true;
					}
				}
			}
		}

		Set<Edge> clean = new HashSet<Edge>();
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {
				if (m[i][j] != 0) {
					clean.add(new Edge(i, j));
				}
			}
		}

		return clean;
	}
}
