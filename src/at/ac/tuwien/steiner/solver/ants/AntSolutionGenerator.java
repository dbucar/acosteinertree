package at.ac.tuwien.steiner.solver.ants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

import at.ac.tuwien.steiner.objects.Ant;
import at.ac.tuwien.steiner.objects.Edge;
import at.ac.tuwien.steiner.objects.Solution;

public class AntSolutionGenerator {

	private static Logger log = Logger.getLogger("at.ac.tuwien.steiner.AntSolutionGenerator");

	private int[][] adjacencyMatrix;
	private Set<Integer> terminals;

	private double alpha;
	private double beta;

	Random rand;

	public AntSolutionGenerator(int[][] adjacencyMatrix, Set<Integer> terminals, double alpha, double beta) {
		this.adjacencyMatrix = adjacencyMatrix;
		this.terminals = terminals;
		this.alpha = alpha;
		this.beta = beta;
		this.rand = new Random(System.currentTimeMillis());
	}

	public Solution generateSolution(double[][] pheromones) {

		Set<Integer> allNodes = new HashSet<Integer>();
		for (int k = 0; k < adjacencyMatrix.length; k++) {
			allNodes.add(k);
		}

		// set the ants into terminals
		List<Ant> ants = initAnts();

		double visibility, pheromoneValue, probability;

		// while not all have been merged
		while (!haveAllMerged(ants)) {

			// each ant one step per iteration
			for (Ant a : ants) {

				// if ant still alive
				if (!a.isStopped()) {

					Map<Integer, Double> probMap = new HashMap<Integer, Double>();
					double probabilitySum = 0;

					int i = a.getCurrentNode();
					for (int j : allNodes) {
						// if neighbors, and not yet visited by this ant
						if (adjacencyMatrix[i][j] > -1 && !(a.getVisitedNodes()).contains(j)) {
							visibility = (double) 1 / adjacencyMatrix[i][j];
							pheromoneValue = pheromones[i][j];
							probability = Math.pow(pheromoneValue, alpha) * Math.pow(visibility, beta);

							probMap.put(j, probability);
							probabilitySum += probability;
						}
					}

					// if no other choice -- stuck -- bactrack
					if (probMap.isEmpty()) {
						List<Integer> reversePath = new ArrayList<Integer>();
						for (int n : a.getVisitedNodes()) {
							reversePath.add(n);
						}
						Collections.reverse(reversePath);
						// removes last node - was stuck there
						reversePath.remove(0);

						// try for all visited nodes, backwards
						for (int node : reversePath) {
							probMap = new HashMap<Integer, Double>();
							probabilitySum = 0;
							for (int j : allNodes) {
								// if neighbors, and not yet visited by this ant
								if (adjacencyMatrix[node][j] > -1 && !(a.getVisitedNodes()).contains(j)) {
									visibility = (double) 1 / adjacencyMatrix[node][j];
									pheromoneValue = pheromones[node][j];
									probability = Math.pow(pheromoneValue, alpha) * Math.pow(visibility, beta);

									probMap.put(j, probability);
									probabilitySum += probability;

								}
							}
							if (!probMap.isEmpty()) {
								a.setCurrentNode(node);
								break;
							}
						}
						// ant stuck
						if (probMap.isEmpty()) {
							System.out.println("Ant " + a.getId() + "can't find a way out");
							a.stop();
						}
					} else {
						int nextNode = select(probMap, probabilitySum);

						// if the ants merge, merge their paths
						Ant a2 = hasMerged(nextNode, ants);
						if (a2 != null && !a2.isStopped()) {
							a.stop();
						}
						a.moveFromTo(a.getCurrentNode(), nextNode, adjacencyMatrix[a.getCurrentNode()][nextNode]);
					}
				}
			}
		}

		Set<Edge> edges = new HashSet<Edge>();
		for (Ant a : ants) {
			edges.addAll(a.getEdgesCrossed());
		}

		edges = removeNonTerminalLeaves(edges);

		int fitnessValue = 0;
		for (Edge e : edges) {
			fitnessValue += adjacencyMatrix[e.getI()][e.getJ()];
		}

		return new Solution(edges, fitnessValue);
	}

	private Set<Edge> removeNonTerminalLeaves(Set<Edge> edges) {

		int[][] m = new int[adjacencyMatrix.length][adjacencyMatrix.length];

		for (Edge e : edges) {
			m[e.getI()][e.getJ()] = 1;
			m[e.getJ()][e.getI()] = 1;
		}

		boolean changed = true;

		while (changed) {
			changed = false;
			for (int i = 0; i < m.length; i++) {
				if (!terminals.contains(i)) {
					int adjVertices = 0;
					int neighbor = -1;
					for (int j = 0; j < m.length; j++) {
						if (m[i][j] != 0) {
							neighbor = j;
							adjVertices++;
						}
					}
					if (adjVertices == 1) {
						m[i][neighbor] = 0;
						m[neighbor][i] = 0;
						changed = true;
					}
				}
			}
		}

		Set<Edge> clean = new HashSet<Edge>();
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j <= i; j++) {
				if (m[i][j] != 0) {
					clean.add(new Edge(i, j));
				}
			}
		}

		return clean;
	}

	// checks if ant has merged with another one
	private Ant hasMerged(int nextNode, List<Ant> ants) {
		for (Ant a : ants) {
			if (a.getVisitedNodes().contains(nextNode))
				return a;
		}
		return null;
	}

	// tests if all have been merged - stopping criteria
	private boolean haveAllMerged(List<Ant> ants) {

		int stoppedCount = 0;

		for (Ant a : ants) {
			if (a.isStopped()) {
				stoppedCount++;
			}
		}

		if (stoppedCount == ants.size() - 1) {
			return true;
		}

		return false;

	}

	private List<Ant> initAnts() {
		List<Ant> ants = new ArrayList<Ant>();
		for (int t : terminals) {
			Ant a = new Ant(t);
			ants.add(a);
		}
		return ants;
	}

	// selection of the next node in an ant path (roulette selection)
	private int select(Map<Integer, Double> probMap, double probabilitySum) {

		if (probMap.isEmpty()) {
			System.out.println("empty");
		}

		double p = rand.nextDouble();
		double tempSum = 0;

		for (int j : probMap.keySet()) {
			tempSum += probMap.get(j) / probabilitySum;
			if (tempSum >= p) {
				return j;
			}
		}

		return -1;
	}

}
