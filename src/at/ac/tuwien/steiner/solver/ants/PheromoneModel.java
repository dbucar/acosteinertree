package at.ac.tuwien.steiner.solver.ants;

import java.util.Set;

import at.ac.tuwien.steiner.objects.Edge;
import at.ac.tuwien.steiner.objects.Solution;

public class PheromoneModel {

	private double t0;
	private double evaporationRate;

	private int[][] adjMatrix;

	public PheromoneModel(int[][] adjMatrix) {
		this.adjMatrix = adjMatrix;
		this.t0 = 0.25;
		this.evaporationRate = 0.5;
	}

	public double[][] initPheromones() {
		double[][] pheros = new double[adjMatrix.length][adjMatrix.length];
		for (int i = 0; i < pheros.length; i++)
			for (int j = 0; j < pheros[i].length; j++)
				pheros[i][j] = t0;
		return pheros;
	}

	public double[][] evaporatePheromones(double[][] pheros) {
		for (int i = 0; i < pheros.length; i++)
			for (int j = 0; j < pheros[i].length; j++) {
				pheros[i][j] = pheros[i][j] * (1 - evaporationRate);
				if (pheros[i][j] < 0.1) {
					pheros[i][j] = 0.1;
				}
			}

		return pheros;
	}

	public double[][] updatePheromones(double[][] pheros, Solution sol) {
		int fitVal = sol.getFitness();
		for (Edge e : sol.getEdges()) {
			pheros[e.getI()][e.getJ()] += Math.sqrt(adjMatrix.length) / fitVal;
		}
		return pheros;
	}
}
