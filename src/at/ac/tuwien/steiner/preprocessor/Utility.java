package at.ac.tuwien.steiner.preprocessor;

public abstract class Utility {
	public static final int X = -1;
	
	public static int findLeaf(int[][] adjacencyMatrix) {
		for(int i = 0; i < adjacencyMatrix.length; i++) {
			int count = 0;
			for(int j = 0; j < adjacencyMatrix[i].length; j++) {
				if(adjacencyMatrix[i][j] != X) {
					count++;
				}
			}
			if(count == 1) {
				return i;
			}
		}
		return -1;
	}
}
