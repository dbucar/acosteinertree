package at.ac.tuwien.steiner.preprocessor;

public class ContractBridge implements PreprocessorAction {
	
	private int affectedVertex;
	private int[] affectedRow;
	
	public ContractBridge(int affectedVertex) {
		this.affectedVertex = affectedVertex;
	}
	
	
	@Override
	public int[][] perform(int[][] adjacencyMatrix) {
		affectedRow = adjacencyMatrix[affectedVertex];
		int[][] newAdjacencyMatrix = new int[adjacencyMatrix.length - 1][adjacencyMatrix[0].length - 1];
		int shiftRow = 0;
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			if(i == affectedVertex) {
				shiftRow++;
				continue;
			}
			int shiftCol = 0;
			for (int j = 0; j < adjacencyMatrix[i].length; j++) {
				if(j == affectedVertex) {
					shiftCol++;
					continue;
				}
				newAdjacencyMatrix[i - shiftRow][j - shiftCol] = adjacencyMatrix[i][j];
			}
		}
		return newAdjacencyMatrix;
	}

	@Override
	public int[][] undo(int[][] adjacencyMatrix) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getAffectedVertex() {
		return affectedVertex;
	}

}
