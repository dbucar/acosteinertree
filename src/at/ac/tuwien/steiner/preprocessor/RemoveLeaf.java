package at.ac.tuwien.steiner.preprocessor;

public class RemoveLeaf implements PreprocessorAction {
	
	private int affectedVertex;
	private int[] affectedRow;
	
	public RemoveLeaf(int affectedVertex) {
		this.affectedVertex = affectedVertex;
	}
	
	public int getAffectedVertex() {
		return affectedVertex;
	}
	
	public static boolean checkIfLeaf(int nodeIndex, int[][] adjacencyMatrix) {
		int sum = 0;
		for(int i = 0; i < adjacencyMatrix.length; i++) {
			if(adjacencyMatrix[nodeIndex][i] != X) { 
				sum += adjacencyMatrix[nodeIndex][i];
			}
		}
		return sum == 1;
	}

	@Override
	public int[][] perform(int[][] adjacencyMatrix) {
		affectedRow = adjacencyMatrix[affectedVertex];
		int[][] newAdjacencyMatrix = new int[adjacencyMatrix.length - 1][adjacencyMatrix[0].length - 1];
		int shiftRow = 0;
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			if(i == affectedVertex) {
				shiftRow++;
				continue;
			}
			int shiftCol = 0;
			for (int j = 0; j < adjacencyMatrix[i].length; j++) {
				if(j == affectedVertex) {
					shiftCol++;
					continue;
				}
				newAdjacencyMatrix[i - shiftRow][j - shiftCol] = adjacencyMatrix[i][j];
			}
		}
		return newAdjacencyMatrix;
	}

	@Override
	public int[][] undo(int[][] adjacencyMatrix) {
		int[][] newAdjacencyMatrix = new int[adjacencyMatrix.length + 1][adjacencyMatrix[0].length + 1];
		
		// System.out.println(affectedRow.length + " " + (adjacencyMatrix.length + 1));
		
		int shiftRow = 0;
		for (int i = 0; i < newAdjacencyMatrix.length; i++) {
			if(i == affectedVertex) {				
				for (int j = 0; j < newAdjacencyMatrix[i].length; j++) {
					// System.out.println(affectedRow.length + " : " + i + " " + j);
					newAdjacencyMatrix[i][j] = affectedRow[j];
				}
				shiftRow++;
				continue;
			}
			
			int shiftCol = 0;
			for (int j = 0; j < newAdjacencyMatrix[i].length; j++) {
				if(j == affectedVertex) {
					newAdjacencyMatrix[i][j] = affectedRow[i];
					
					shiftCol++;
					continue;
				}
				newAdjacencyMatrix[i][j] = adjacencyMatrix[i - shiftRow][j - shiftCol];
			}
		}
		return newAdjacencyMatrix;
	}
	
}
