package at.ac.tuwien.steiner.preprocessor.test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;

import junit.framework.TestCase;

import org.junit.Test;

import at.ac.tuwien.steiner.preprocessor.Preprocessor;
import at.ac.tuwien.steiner.preprocessor.PreprocessorAction;
import at.ac.tuwien.steiner.preprocessor.RemoveLeaf;
import at.ac.tuwien.steiner.preprocessor.Utility;

public class RemoveLeafTest extends TestCase {
	
	public static final int X = -1;

	@Test
	public void test() {
		int[][] adjacencyMatrix = {
				{X, X, 1, X, X, X, X},
				{X, X, 1, X, X, X, X}, 
				{1, 1, X, 1, X, X, X}, 
				{X, X, 1, X, 1, 1, 1}, 
				{X, X, X, 1, X, 1, X}, 
				{X, X, X, 1, 1, X, X},
				{X, X, X, 1, X, X, X},
			};
		
//		for (int[] is : adjacencyMatrix) {
//			for (int i : is) {
//				System.out.print(i == -1 ? "X " : i + " ");
//			}
//			System.out.println();
//		}
//		System.out.println();
		
		
		Stack<PreprocessorAction> actions = new Stack<PreprocessorAction>();
		
		int leaf = -1;
		assertTrue(Utility.findLeaf(adjacencyMatrix) != -1);
		while((leaf = Utility.findLeaf(adjacencyMatrix)) != -1) {
			PreprocessorAction action = new RemoveLeaf(leaf);
			actions.push(action);
			adjacencyMatrix = action.perform(adjacencyMatrix);
//			for (int[] is : adjacencyMatrix) {
//				for (int i : is) {
//					System.out.print(i == -1 ? "X " : i + " ");
//				}
//				System.out.println();
//			}
//			System.out.println();
		}
		
		while(!actions.isEmpty()) {
			PreprocessorAction action = actions.pop();
			adjacencyMatrix = action.undo(adjacencyMatrix);
//			for (int[] is : adjacencyMatrix) {
//				for (int i : is) {
//					System.out.print(i == -1 ? "X " : i + " ");
//				}
//				System.out.println();
//			}
//			System.out.println();
		}
		
		
		
		Set<Integer> terminals = new HashSet<Integer>();
		terminals.add(0);
		terminals.add(3);
		terminals.add(4);
		
		Preprocessor p = new Preprocessor(adjacencyMatrix, terminals);
		Object[] result = p.preprocess();
		int[][] resultingMatrix = (int[][]) result[0];
		System.out.println("++++++++++++++++++++++++++++++++++++++++++");
//		for (int i = 0; i < resultingMatrix.length; i++) {
//			for (int j = 0; j < resultingMatrix[i].length; j++) {
//				System.out.print(resultingMatrix[i][j] == X ? "X " : resultingMatrix[i][j] + " ");
//			}
//			System.out.println();
//		}
//		System.out.println();
		
		Set<Integer> edge = new HashSet<Integer>();
		edge.add(0);
		edge.add(1);
		Set<Set<Integer>> solution = new HashSet<Set<Integer>>();
		solution.add(edge);
		
		Collection<Set<Integer>> solution2 = p.revert(resultingMatrix, solution);
		for (Set<Integer> set : solution2) {
			for (Integer integer : set) {
				System.out.print(integer + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

}
