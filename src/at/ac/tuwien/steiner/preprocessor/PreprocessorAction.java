package at.ac.tuwien.steiner.preprocessor;

public interface PreprocessorAction {
	public static final int X = -1;
	public int[][] perform(int[][] adjacencyMatrix);
	public int[][] undo(int[][] adjacencyMatrix);
	public int getAffectedVertex();
}
