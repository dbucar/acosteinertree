package at.ac.tuwien.steiner.preprocessor;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class Preprocessor {
	private Stack<PreprocessorAction> actionStack;
	private Stack<Set<Integer>> terminalStack;
	
	private int[][] adjacencyMatrix;
	private Set<Integer> terminals;
	
	public Preprocessor(int[][] adjacencyMatrix, Set<Integer> terminals) {
		this.adjacencyMatrix = adjacencyMatrix;
		this.terminals = terminals;
	}
	
	public Object[] preprocess() {
		actionStack = new Stack<PreprocessorAction>();
		terminalStack = new Stack<Set<Integer>>();
		
		
		int[][] newAdjacencyMatrix = adjacencyMatrix;
		Set<Integer> oldTerminals = new HashSet<Integer>(terminals);
		
		terminalStack.push(oldTerminals);
		while(true) {
			boolean actionPerformed = false;
			
			System.out.println(oldTerminals);

			terminalStack.push(oldTerminals);
			
			int leaf = findLeaf(newAdjacencyMatrix);
			if(leaf != - 1) {
				Set<Integer> oldTerminals2 = new HashSet<Integer>(oldTerminals);
				
				System.out.println(leaf);
				if(oldTerminals.contains(leaf)) {
					oldTerminals2.remove(leaf);
					oldTerminals2.add(neighborOfLeaf(newAdjacencyMatrix, leaf));
				}
				
				Set<Integer> newTerminals = new HashSet<Integer>();
				for(Integer e : oldTerminals2) {
					if(e > leaf) {
						newTerminals.add(e - 1);
					} else {
						newTerminals.add(e);
					}
				}
				
				PreprocessorAction action = new RemoveLeaf(leaf);
				
				oldTerminals = newTerminals;
				actionStack.push(action);
				
				newAdjacencyMatrix = action.perform(newAdjacencyMatrix);
				
				actionPerformed = true;
			}
			
			if(!actionPerformed) {
				break;
			}
		}
		return new Object[] {newAdjacencyMatrix, oldTerminals};
	}
	
	public Collection<Set<Integer>> revert(int[][] adjacencyMatrix, Collection<Set<Integer>> solution) {
		int[][] newAdjacencyMatrix = adjacencyMatrix;
		
		terminalStack.pop();
		
		while(!actionStack.isEmpty()) {
			PreprocessorAction action = actionStack.pop();
			Set<Integer> terminals = terminalStack.pop();
			
			newAdjacencyMatrix = action.undo(newAdjacencyMatrix);
			System.out.println(terminals + " : " + action.getAffectedVertex() + "-" + neighborOfLeaf(newAdjacencyMatrix, action.getAffectedVertex()));
			if(action instanceof RemoveLeaf) {
				
				System.out.print("!!! ");
				for (Set<Integer> set : solution) {
					System.out.print(set + ", ");
				}
				System.out.println();
				
				int leaf = action.getAffectedVertex();
				Set<Set<Integer>> newSolution = new HashSet<Set<Integer>>();
				for (Set<Integer> set : solution) {
					System.out.print("> " + set);
					Set<Integer> newEdge = new HashSet<Integer>();
					for (Integer e : set) {
						if(e >= leaf) {
							newEdge.add(e + 1);
						} else {
							newEdge.add(e);
						}
					}
					System.out.println(" " + newEdge);
					newSolution.add(newEdge);
				}
				
				System.out.print("??? ");
				for (Set<Integer> set : newSolution) {
					System.out.print(set + ", ");
				}
				System.out.println();
				
				solution.clear();
				solution.addAll(newSolution);
				
				if(terminals.contains(action.getAffectedVertex())) {
					Set<Integer> newEdge = new HashSet<Integer>();
					newEdge.add(action.getAffectedVertex());
					newEdge.add(neighborOfLeaf(newAdjacencyMatrix, action.getAffectedVertex()));
					solution.add(newEdge);
					// System.out.println("NEW: " + newEdge);
				}
				for (Set<Integer> set : solution) {
					System.out.print(set + ", ");
				}
				System.out.println();
			}
			
//			for (int i = 0; i < newAdjacencyMatrix.length; i++) {
//				for (int j = 0; j < newAdjacencyMatrix[i].length; j++) {
//					System.out.print(newAdjacencyMatrix[i][j] == -1 ? "X " : newAdjacencyMatrix[i][j] + " ");
//				}
//				System.out.println();
//			}
			System.out.println();
			System.out.println("------------------------------------------");
		}
		return solution;
	}
	
	private int findLeaf(int[][] adjacencyMatrix) {
		for(int i = 0; i < adjacencyMatrix.length; i++) {
			int count = 0;
			for(int j = 0; j < adjacencyMatrix[i].length; j++) {
				if(adjacencyMatrix[i][j] != PreprocessorAction.X) {
					count++;
				}
			}
			if(count == 1) {
				return i;
			}
		}
		return -1;
	}
	
	private int findBridge(int[][] adjacencyMatrix) {
		for(int i = 0; i < adjacencyMatrix.length; i++) {
			int count = 0;
			for(int j = 0; j < adjacencyMatrix[i].length; j++) {
				if(adjacencyMatrix[i][j] != PreprocessorAction.X) {
					count++;
				}
			}
			if(count == 2) {
				return i;
			}
		}
		return -1;
	}
	
	private int neighborOfLeaf(int[][] adjacencyMatrix, int leaf) {
		for(int j = 0; j < adjacencyMatrix[leaf].length; j++) {
			if(adjacencyMatrix[leaf][j] != PreprocessorAction.X) {
				return j;
			}
		}
		return -1;
	}
}
